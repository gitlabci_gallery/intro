The purpose of the gallery is to get a set of example projects showing how to use gitlab-ci in a development process. It is not exhaustive and any new example is welcome.


If you have a project you want to add in the gallery just creat an issue for the `intro` project with the following informations:

* The name and the URL of your project
* The group you think it should belongs to (or propose a new group if no group matches).

Your project must be simple and contain:

* a README.md that explains the purpose, how it works and how to make it work (specific parameters, variables, runners...);
* a filled project description (in `Settings/General/Naming, topics, avatar`) that quickly explains the purpose of the project.
 
To enable people to reuse your project without any constraints, we strongly recommend that you add a BSD 3 license to your project. You can simply use Gitlab's `Add license` button.

The members of the gitlabci-gallery group will then review your issue and integrate your project as soon as possible. 

Thanks! 

