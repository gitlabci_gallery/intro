---
theme: gaia
_class: lead
paginate: true
backgroundColor: white
---

**gitlabci_gallery**
(et un peu d'orchestration)

https://gitlab.inria.fr/gitlabci_gallery

---

# Illustrer des usages courants

- gitlab-pages: hugo, jupyter, sphinx
  https://gitlab.inria.fr/gitlabci_gallery/pages

- `terraform` pour spécifier (et versionner !) son infrastructure
  sur CloudStack
  https://gitlab.inria.fr/gitlabci_gallery/orchestration/terraform

- matrices de tâches
  https://gitlab.inria.fr/gitlabci_gallery/pipelines/parallel_jobs

- tests de performance automatique avec slurm
  https://gitlab.inria.fr/gitlabci_gallery/testing/supercomputer-slurm

---

# Utiliser `docker build` avec gitlab-CI

Un motif courant :

- une première tâche fait `docker build` seulement lorsque
  `Dockerfile` change,

- les autres tâches utilisent cette image `docker`.

Un exemple :

https://gitlab.inria.fr/gitlabci_gallery/latex/latex-beamer

---

# Modèle pour docker build

https://gitlab.inria.fr/inria-ci/docker/-/tree/main/register-dockerfile

Permet de moins se répéter dans le `.gitlab-ci.yml` pour
mettre en place la tâche qui fait `docker build`.

Gère proprement des subtilités :
que se passe-t-il si entre deux commits sur la branche `a` qui
ne changent pas le `Dockerfile`, l’environnement du `Dockerfile`
est modifié de manière incompatible sur la branche `b` ?

Solution : une image par branche, qu’il faut nettoyer
automatiquement lorsque ces branches disparaissent.

---

# Terraform

- pour spécifier (et versionner !) son infrastructure sur CloudStack
  https://gitlab.inria.fr/gitlabci_gallery/orchestration/terraform

- pour déployer une infrastructure au début d’un pipeline pour
  exécuter des tâches et tout nettoyer à la fin du pipeline
  (en permettant l'exécution de pipelines concurrents !)
  https://gitlab.inria.fr/gitlabci_gallery/orchestration/terraform-dynamic

---

# Contributions et commentaires bienvenus

- Espace d'échanges d'exemples pour apprendre et pour réutiliser

- Des exemples jouets pour expérimenter de nouvelles façon de
  faire, tester des améliorations.

- Vers d'autres galleries : cmake, C++, tous les trucs qu'on a
  mentionnés en distinguant des façons de faire « modernes ».
